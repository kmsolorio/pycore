from nose.tools import *

# In this testing module, we are going to test some of the functions
# that are built into Python.  Since these functions are part of the
# language, there is no need to import anything (outside of testing tools)
# into the tests.


def test_abs_provides_the_absolute_value_of_a_positive_integer():
  assert_equal(3, abs(3))

def test_abs_provides_the_absolute_value_of_a_negative_integer():
  assert_equal(3, abs(-3))

def test_all_returns_true_if_all_values_of_an_iterable_are_truthy():
  assert all([1, 2, 3, 'a', 'b', 'c'])

def test_all_returns_true_if_iterable_is_empty():
  assert all([])

def test_all_returns_false_if_iterable_contains_a_false_item():
  assert not all([1, 2, None, 3, False])

def test_any_returns_true_if_any_element_of_iterable_evaluates_truthy():
  assert any([False, None, 0, 'a'])

def test_any_returns_false_if_all_elements_eveluate_to_falsy():
  assert not any([False, None, 0])

def test_any_returns_false_if_iterable_is_empty():
  assert not any([])

def test_bin_returns_a_binary_string():
  assert isinstance(bin(4), basestring)

def test_bool_returns_true_if_passed_in_value_is_truthy():
  assert bool(1)

def test_bool_returns_false_if_passed_in_value_is_falsy():
  assert not bool(None)